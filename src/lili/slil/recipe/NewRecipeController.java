/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lili.slil.recipe;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author orban
 */
public class NewRecipeController extends Recipe implements Initializable{


    @FXML
    private TextField ing1;
    @FXML
    private TextField ing2;
    @FXML
    private TextField ing3;
    @FXML
    private TextField ing4;
    @FXML
    private TextField ing5;
    @FXML
    private TextField ing7;
    @FXML
    private TextField ing8;
    @FXML
    private RadioButton ButtonSoup;
    @FXML
    private RadioButton ButtonMainCourse;
    @FXML
    private RadioButton ButtonDessert;
    @FXML
    private RadioButton ButtonSalad;
    @FXML
    private RadioButton ButtonOther;
    @FXML
    private ComboBox<?> TimeBox;
    @FXML
    private Button MainMenuPressed;
    @FXML
    private TextArea MethodTextArea;
    @FXML
    private Button PutInPressed;
    @FXML
    private TextField NameTextField;
    
    public NewRecipeController()
    {
        
    }
    
    
    
    @FXML
    public void MainMenu(ActionEvent event) throws IOException
    {
        Parent parent = FXMLLoader.load(getClass().getResource("main.fxml"));
        Scene scene = new Scene(parent);
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(scene);
        window.show();
    }
    
    public void PutIn(ActionEvent event) throws IOException
    {
        Parent parent = FXMLLoader.load(getClass().getResource(""));
        Scene scene = new Scene(parent);
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(scene);
        window.show();
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
