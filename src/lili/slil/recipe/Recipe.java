/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lili.slil.recipe;

import java.io.File;

/**
 *
 * @author orban
 */
public class Recipe{
    private String[] ingredients;
    private String name;
    private int diflevel, time;
    private boolean exclusively, soup, maincourse, dessert, salad, other;
    private File method;
    
    public Recipe()
    {
        
    }
    public Recipe(String[] ingredients, String name, int diflevel, int time, boolean exclusively, boolean soup, 
                  boolean maincourse, boolean dessert, boolean salad, boolean other, File method) 
    {
        this.ingredients = ingredients;
        this.name = name;
        this.diflevel = diflevel;
        this.time = time;
        this.exclusively= exclusively;
        this.soup = soup;
        this.maincourse = maincourse;
        this.dessert = dessert;
        this.salad = salad;
        this.other = other;
        this.method = method;
    }

    public boolean isSoup() {
        return soup;
    }

    public void setSoup(boolean soup) {
        this.soup = soup;
    }

    public boolean isMaincourse() {
        return maincourse;
    }

    public void setMaincourse(boolean maincourse) {
        this.maincourse = maincourse;
    }

    public boolean isDessert() {
        return dessert;
    }

    public void setDessert(boolean dessert) {
        this.dessert = dessert;
    }

    public boolean isSalad() {
        return salad;
    }

    public void setSalad(boolean salad) {
        this.salad = salad;
    }

    public boolean isOther() {
        return other;
    }

    public void setOther(boolean other) {
        this.other = other;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String[] getIngredients() {
        return ingredients;
    }

    public void setIngredients(String[] ingredients) {
        this.ingredients = ingredients;
    }

    public int getDiflevel() {
        return diflevel;
    }

    public void setDiflevel(int diflevel) {
        this.diflevel = diflevel;
    }

    public boolean isExclusively() {
        return exclusively;
    }

    public void setExclusively(boolean exclusively) {
        this.exclusively = exclusively;
    }

    public File getMethod() {
        return method;
    }

    public void setMethod(File method) {
        this.method = method;
    }
    
}
