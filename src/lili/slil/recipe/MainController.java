/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lili.slil.recipe;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author orban
 */
public class MainController implements Initializable {

    @FXML
    private Button DetailedSearchPressed;
    @FXML
    private Button SearchByNaemPressed;
    @FXML
    private Button NewRecipePressed;

    
    
    @FXML
    public void DetailedSearch(ActionEvent event) throws IOException
    {
        Parent parent = FXMLLoader.load(getClass().getResource("detailed.fxml"));
        Scene scene = new Scene(parent);
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(scene);
        window.show();
    }
    
    @FXML
    public void SearchByName(ActionEvent event) throws IOException
    {
        Parent parent = FXMLLoader.load(getClass().getResource("searchbyname.fxml"));
        Scene scene = new Scene(parent);
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(scene);
        window.show();
    }
    
    @FXML
    public void NewRecipe(ActionEvent event) throws IOException
    {
        Parent parent = FXMLLoader.load(getClass().getResource("new recipe.fxml"));
        Scene scene = new Scene(parent);
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(scene);
        window.show();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
